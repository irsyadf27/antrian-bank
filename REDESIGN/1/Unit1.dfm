object Form1: TForm1
  Left = 200
  Top = 141
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Aplikasi Antrian Bank'
  ClientHeight = 307
  ClientWidth = 625
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 225
    Height = 169
    Caption = ' Meja 1 '
    TabOrder = 0
    object Label1: TLabel
      Left = 40
      Top = 48
      Width = 142
      Height = 75
      Caption = 'B-000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -64
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 240
    Top = 8
    Width = 225
    Height = 169
    Caption = ' Meja 2 '
    TabOrder = 1
    object Label2: TLabel
      Left = 40
      Top = 48
      Width = 139
      Height = 75
      Caption = 'P-000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -64
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GroupBox3: TGroupBox
    Left = 472
    Top = 8
    Width = 145
    Height = 65
    Caption = ' Volume '
    TabOrder = 2
    object Volume: TTrackBar
      Left = 8
      Top = 24
      Width = 129
      Height = 25
      Max = 20
      TabOrder = 0
      ThumbLength = 18
      OnChange = VolumeChange
    end
  end
  object GroupBox4: TGroupBox
    Left = 8
    Top = 184
    Width = 225
    Height = 113
    Caption = ' Panggil Antrian  '
    TabOrder = 3
    object BitBtn1: TBitBtn
      Left = 16
      Top = 24
      Width = 89
      Height = 73
      Caption = 'Meja 1'
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn1Click
      Glyph.Data = {
        D60B0000424DD60B00000000000036000000280000001F0000001F0000000100
        180000000000A00B0000232E0000232E00000000000000000000FFFFFFF5F6E3
        D6DD8AD2D97ED2D97ED2D97ED2D97ED2D97ED2D97ED2D97ED2D97ED2D97ED2D9
        7ED2D97ED2D97ED2D97ED2D97ED2D97ED2D97ED2D97ED2D97ED2D97ED2D97ED2
        D97ED2D97ED2D97ED2D97ED2D97ED2D97ED9DF93F7F8E8000000F8F9ECAEBC18
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500ADBB15000000DAE095A6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500AAB90DDFE4A4B8C435A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500B1BE20EFF1D2FFFFFFC5CF5BA6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500BBC63CF7F8E9FFFFFFFFFFFFC5CF5BA6B500A6B500BDC842E0E5A7A8
        B706A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00C8D161FCFDF9FFFFFFFFFFFFFFFFFFC5CF5BA6B500A6B500B3BF25FCFDF9CE
        D674A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500B1BE1FD3DB83D5DC89D5DC87D5DC87DFE4
        A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5CF5BBBC73EE3E8B0A7B502DAE097F8
        F9ECA8B706A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500D3DA82FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5CF5BBAC539FFFFFFC0CB4CB5C22CFF
        FFFFC0CA4BA6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500D4DC86FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5CF5BA7B603F9FAEED6DD8BA6B500F7
        F8EAD1D97DA6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500D4DC86FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5CF5BA6B500E2E7ACE8ECBFA6B500ED
        F0CED6DD8BA6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500D4DC86FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5CF5BA6B500E9EDC2E3E7AFA6B500EF
        F1D1D0D87AA6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500D4DC86FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5CF5BABB910FDFDFBCFD777A7B603FB
        FCF5C4CE58A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500CED674FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5CF5BC3CD54FDFEFCB3C027C0CA4AFF
        FFFFB0BD1DA6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A9B709BEC945C1CB4DC0CB4CC0CB4CC7D0
        5FF8F9EDFFFFFFFFFFFFFFFFFFFFFFFFC5CF5BB2BF24C2CC52A6B500E7EBBCE8
        EBBDA6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00B2BF24F0F3D6FFFFFFFFFFFFFFFFFFC5CF5BA6B500A6B500BAC63BFEFEFEBC
        C841A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500ABB90FE5EAB7FFFFFFFFFFFFC5CF5BA6B500A6B500B0BD1CC7D05EA6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A7B603D8DE8FFFFFFFC5CF5BA6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500C3CD54AEBC18A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D2D97EA6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000D5DC87A6B500
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500000000F3F5DEACBA12
        A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B5
        00A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6B500A6
        B500A6B500A6B500A6B500A6B500A6B500A6B500ABB910000000}
      Layout = blGlyphTop
    end
    object BitBtn2: TBitBtn
      Left = 120
      Top = 24
      Width = 89
      Height = 73
      Caption = 'Meja 2'
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BitBtn2Click
      Glyph.Data = {
        D60B0000424DD60B00000000000036000000280000001F0000001F0000000100
        180000000000A00B0000232E0000232E00000000000000000000FFFFFFF8F2E4
        E2CC91DFC786DFC786DFC786DFC786DFC786DFC786DFC786DFC786DFC786DFC7
        86DFC786DFC786DFC786DFC786DFC786DFC786DFC786DFC786DFC786DFC786DF
        C786DFC786DFC786DFC786DFC786DFC786E4D09AF9F5E9000000FAF6EDC79A27
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C69924000000E5D19CC19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C4961DE8D7AACEA742C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C89E2EF4EBD4FFFFFFD7B765C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010CFAA48F9F5EAFFFFFFFFFFFFD7B765C19010C19010D1AD4EE9D8ACC2
        9316C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10D8BA6BFDFCF9FFFFFFFFFFFFFFFFFFD7B765C19010C19010CAA033FDFCF9DD
        C27DC19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C89D2DE0C98BE2CB90E1CA8EE1CA8EE8D7
        A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7B765D0AB4AEBDCB5C19112E5D19DFA
        F6EDC29316C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010E0C88AFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7B765CFA946FFFFFFD3B157CBA33AFF
        FFFFD3B056C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010E1CA8EFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7B765C19113FAF7EFE2CC92C19010F9
        F5EBDFC685C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010E1CA8EFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7B765C19010EADBB1EFE3C3C19010F3
        E9D1E2CC92C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010E1CA8EFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7B765C19010F0E4C6EBDCB4C19010F3
        EBD4DEC582C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010E1CA8EFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7B765C5971FFEFDFBDEC480C19113FC
        FAF5D6B663C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010DDC27DFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7B765D5B45FFEFDFCCAA135D3B056FF
        FFFFC89D2CC19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C39419D1AE51D3B158D3B157D3B157D8B9
        69FAF7EEFFFFFFFFFFFFFFFFFFFFFFFFD7B765C9A032D5B35DC19010EEE1C0EF
        E2C1C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C9A032F5EDD8FFFFFFFFFFFFFFFFFFD7B765C19010C19010CFAA48FEFEFED0
        AC4DC19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C4961EEDDFBBFFFFFFFFFFFFD7B765C19010C19010C89C2BD8B968C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19113E3CE96FFFFFFD7B765C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010D5B45FC79A27C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000DFC786C19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000E1CA8EC19010
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C19010000000F7F0E0C59821
        C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C190
        10C19010C19010C19010C19010C19010C19010C19010C19010C19010C19010C1
        9010C19010C19010C19010C19010C19010C19010C5971F000000}
      Layout = blGlyphTop
    end
  end
  object GroupBox5: TGroupBox
    Left = 240
    Top = 184
    Width = 225
    Height = 113
    Caption = ' Ambil Antrian '
    TabOrder = 4
    object BitBtn3: TBitBtn
      Left = 120
      Top = 24
      Width = 89
      Height = 73
      Caption = 'Personal'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn3Click
      Glyph.Data = {
        D60B0000424DD60B00000000000036000000280000001F0000001F0000000100
        180000000000A00B0000232E0000232E00000000000000000000FDFEFECCE2FA
        A3CCF7A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CC
        F6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2
        CCF6A2CCF6A2CCF6A2CCF6A2CCF6A2CCF6A3CCF7CCE2FA000000CCE3FA489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489A
        EF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A3CCF7489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489A
        EF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEFD9EAFBFCFDFEFCFDFEFCFDFEFCFDFEFCFDFEFCFDFEFCFDFEFCFDFEFCFD
        FEFCFDFEFCFDFEFCFDFEFCFDFEFCFDFEFCFDFEFCFDFEFCFDFEFCFDFEFCFDFEFC
        FDFEFCFDFEFCFDFEFCFDFEFCFDFEBBD9F9489AEF489AEF000000A2CCF6489AEF
        489AEFCDE3FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFADD2F7489AEF489AEF000000A2CCF6489AEF
        489AEFB6D6F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF96C5F5489AEF489AEF000000A2CCF6489AEF
        489AEF8ABEF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF68ABF1489AEF489AEF000000A2CCF6489AEF
        489AEF55A1F0F6FAFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFE4F0FC499AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEFAED2F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF8FC1F5489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF5CA5F0FAFCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFE8F2FD4C9CEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEFA9CFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF8ABEF4489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF4A9BEFC9E1FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFAED2F7489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF529FEFDEEDFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFF4F9FED4E7FBBFDBF9C0DCF9D7E9FBF9FBFEFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFC8E1FA4A9BEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF55A1F0CFE4FAFFFFFFFFFFFFE8F2FD84BB
        F44D9CEF489AEF489AEF489AEF489AEF53A0F092C3F5EBF4FDFFFFFFFFFFFFB9
        D8F84D9DEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF4A9BEF97C5F5AED2F7519FEF4D9D
        EF8BBFF4C0DCF9D7E9FBD4E7FBB9D8F87FB8F3499AEF529FEFB6D6F881B9F448
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF6DAEF2DEED
        FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9EAFB63A8F1489AEF489AEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF79B5F3FCFDFEFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F8FD67ABF1489AEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF57A2F0ECF4FDFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F0FC4D9CEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEFB1D4F8FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF93C3F548
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF499AEFF2F7FDFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD1E5FB48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF5AA4F0FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F7FD48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF61A7F1FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FBFE48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF4F9EEFFEFEFEFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4F0FC48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEFD6E8FBFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3D5F848
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF7FB8F3FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFDFE66AAF148
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEFB7D7F8FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7CEF7489AEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF519FEFC0DCF9FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5D6F84A9BEF489AEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF87BD
        F4EAF3FDFFFFFFFFFFFFFFFFFFFFFFFFE0EEFC84BBF4489AEF489AEF489AEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A2CCF6489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489A
        EF499AEF67ABF17CB7F37AB6F362A8F1489AEF489AEF489AEF489AEF489AEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000A3CCF7489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489A
        EF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000CCE3FA489AEF
        489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489A
        EF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF48
        9AEF489AEF489AEF489AEF489AEF489AEF489AEF489AEF000000}
      Layout = blGlyphTop
    end
    object BitBtn4: TBitBtn
      Left = 16
      Top = 24
      Width = 89
      Height = 73
      Caption = 'Bisnis'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BitBtn4Click
      Glyph.Data = {
        D60B0000424DD60B00000000000036000000280000001F0000001F0000000100
        180000000000A00B0000232E0000232E00000000000000000000FDFEFEC2EEDC
        92E0C191E0C191E0C191E0C191E0C191E0C191E0C191E0C191E0C191E0C191E0
        C191E0C191E0C191E0C191E0C191E0C191E0C191E0C191E0C191E0C191E0C191
        E0C191E0C191E0C191E0C191E0C191E0C192E0C1C2EEDC000000C3EEDD25C284
        25C28425C28425C28425C28425C28425C28425C28425C28425C28425C28425C2
        8425C28425C28425C28425C28425C28425C28425C28425C28425C28425C28425
        C28425C28425C28425C28425C28425C28425C28425C28400000092E0C125C284
        25C28425C28425C28425C28425C28425C28425C28425C28425C28425C28425C2
        8425C28425C28425C28425C28425C28425C28425C28425C28425C28425C28425
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C284D1F2E5FBFEFDFBFEFDFBFEFDFBFEFDFBFEFDFBFEFDFBFEFDFBFEFDFBFE
        FDFBFEFDFBFEFDFBFEFDFBFEFDFBFEFDFBFEFDFBFEFDFBFEFDFBFEFDFBFEFDFB
        FEFDFBFEFDFBFEFDFBFEFDFBFEFDAEE8D125C28425C28400000091E0C125C284
        25C284C4EEDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF9DE3C825C28425C28400000091E0C125C284
        25C284A8E6CDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF82DCB825C28425C28400000091E0C125C284
        25C28474D8B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF4BCC9925C28425C28400000091E0C125C284
        25C28435C68DF4FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFDFF6ED26C28425C28425C28400000091E0C125C284
        25C28425C2849EE4C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF7AD9B425C28425C28425C28400000091E0C125C284
        25C28425C2843DC891F9FDFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFE4F7F02AC38625C28425C28425C28400000091E0C125C284
        25C28425C28425C28498E2C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF74D8B025C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28428C285BFEDDAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF9EE4C825C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28431C58BD8F4E9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFF3FBF8CBF0E2B3E9D4B4EAD5D0F1E4F8FDFBFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFBEECDA28C28525C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28435C68DC5EFDEFFFFFFFFFFFFE4F7F06DD6
        AC2AC38725C28425C28425C28425C28432C58B7DDAB5E8F8F2FFFFFFFFFFFFAC
        E7D02BC38725C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28427C28583DCB99EE4C830C58A2BC3
        8775D8B1B4EAD5D0F1E4CBF0E2ACE7D066D4A826C28431C58BA8E6CD69D5AA25
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28425C28425C28425C28452CE9DD8F4
        E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD1F2E545CB9625C28425C28425
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28425C28425C2845FD2A5FBFEFDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FAF64ACC9925C28425
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28425C28436C78EE8F8F2FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEF5EC2AC38725
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28425C284A3E5CBFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7EDBB625
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28426C284EFFAF6FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8EFE025
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C2843BC890FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEFAF525
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28442CA94FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FCFA25
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C2842EC489FEFEFEFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF6ED25
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28425C284CFF1E4FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4E5CC25
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28425C28467D4A9FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFD48CC9825
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28425C28425C284A9E7CEFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF96E1C325C28425
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28425C28425C28430C58AB4EAD5FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7E6CD27C28525C28425
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28425C28425C28425C28425C28470D7
        AEE6F8F1FFFFFFFFFFFFFFFFFFFFFFFFDAF4EA6CD6AC25C28425C28425C28425
        C28425C28425C28425C28425C28425C28425C28425C28400000091E0C125C284
        25C28425C28425C28425C28425C28425C28425C28425C28425C28425C28425C2
        8426C2844ACC9964D3A761D2A644CA9525C28425C28425C28425C28425C28425
        C28425C28425C28425C28425C28425C28425C28425C28400000092E0C125C284
        25C28425C28425C28425C28425C28425C28425C28425C28425C28425C28425C2
        8425C28425C28425C28425C28425C28425C28425C28425C28425C28425C28425
        C28425C28425C28425C28425C28425C28425C28425C284000000C3EEDD25C284
        25C28425C28425C28425C28425C28425C28425C28425C28425C28425C28425C2
        8425C28425C28425C28425C28425C28425C28425C28425C28425C28425C28425
        C28425C28425C28425C28425C28425C28425C28425C284000000}
      Layout = blGlyphTop
    end
  end
end
