unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, mmsystem;

const
   MaxHeap = 999;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Volume: TTrackBar;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    procedure VolumeChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  DataHeap = record
        Nomor : integer;
        Prioritas : integer;
        Tipe : char;
     end;
  ArrayHeap = array[1..MaxHeap] of DataHeap;
var
  Form1: TForm1;
  Heap : ArrayHeap;
  length, Nomor : integer;
  data : DataHeap;
  angka : string;

implementation

{$R *.dfm}

procedure TForm1.VolumeChange(Sender: TObject);
begin
  Case Volume.Position of
     1: waveOutSetVolume(0,$00000000);
     2: waveOutSetVolume(0,$10001000);
     3: waveOutSetVolume(0,$20002000);
     4: waveOutSetVolume(0,$30003000);
     5: waveOutSetVolume(0,$40004000);
     6: waveOutSetVolume(0,$50005000);
     7: waveOutSetVolume(0,$60006000);
     8: waveOutSetVolume(0,$70007000);
     9: waveOutSetVolume(0,$80008000);
     10:waveOutSetVolume(0,$90009000);
     11:waveOutSetVolume(0,$A000A000);
     12:waveOutSetVolume(0,$B000B000);
     13:waveOutSetVolume(0,$C000C000);
     14:waveOutSetVolume(0,$D000D000);
     15:waveOutSetVolume(0,$E000E000);
     16:waveOutSetVolume(0,$F000F000);
     17:waveOutSetVolume(0,$FF00FF00);
     18:waveOutSetVolume(0,$FFF0FFF0);
     19:waveOutSetVolume(0,$FFFFFFFF);
     20:waveOutSetVolume(0,$FFFFFFFF);
  end;
end;

procedure sebutangka(angka : char);
   begin
      case angka of
         '0' : PlaySound('suara/nol.wav', 0, SND_SYNC or SND_FILENAME);
         '1' : PlaySound('suara/satu.wav', 0, SND_SYNC or SND_FILENAME);
         '2' : PlaySound('suara/dua.wav', 0, SND_SYNC or SND_FILENAME);
         '3' : PlaySound('suara/tiga.wav', 0, SND_SYNC or SND_FILENAME);
         '4' : PlaySound('suara/empat.wav', 0, SND_SYNC or SND_FILENAME);
         '5' : PlaySound('suara/lima.wav', 0, SND_SYNC or SND_FILENAME);
         '6' : PlaySound('suara/enam.wav', 0, SND_SYNC or SND_FILENAME);
         '7' : PlaySound('suara/tujuh.wav', 0, SND_SYNC or SND_FILENAME);
         '8' : PlaySound('suara/delapan.wav', 0, SND_SYNC or SND_FILENAME);
         '9' : PlaySound('suara/sembilan.wav', 0, SND_SYNC or SND_FILENAME);
      end;
   end;

procedure suara(meja : integer;tipe : char;nomor : string);
   var
      i : integer;
   begin
      PlaySound('suara/nomor-urut.wav', 0, SND_SYNC or SND_FILENAME);
      if(tipe = 'B') then
         PlaySound('suara/B.wav', 0, SND_SYNC or SND_FILENAME)
      else
         if(tipe = 'P') then
            PlaySound('suara/P.wav', 0, SND_SYNC or SND_FILENAME);
      for i:=1 to 3 do
         sebutangka(nomor[i]);

      PlaySound('suara/meja.wav', 0, SND_SYNC or SND_FILENAME);

      if(meja = 1) then
         PlaySound('suara/satu.wav', 0, SND_SYNC or SND_FILENAME)
      else
         if(meja = 2) then
            PlaySound('suara/dua.wav', 0, SND_SYNC or SND_FILENAME);
   end;

procedure max_heapify(var Heap : ArrayHeap; i :integer; n : integer);
   var
      left , right, largest : integer;
      element : DataHeap;

   begin
      left := 2 * i;
      right := 2 * i + 1;
      if((left <= n) and (Heap[left].Prioritas > Heap[i].Prioritas)) then
         largest := left
      else
         largest := i;

      if((right <= n) and (Heap[right].Prioritas > Heap[largest].Prioritas)) then
         largest := right;

      if(largest <> i) then
         begin
            element := Heap[i];
            Heap[i] := Heap[largest];
            Heap[largest] := element;
            max_heapify(Heap, largest, n);
         end; { EndIf }
   end; { EndProcedure }

function maximum(Heap : ArrayHeap) : DataHeap;
   begin
      maximum := Heap[1];
   end; { EndFunction }

function extract_maximum(var Heap : ArrayHeap; var length : integer) : DataHeap;
   var
      max : DataHeap;

   begin
      if(length > 0) then
         begin
            max := Heap[1];
            Heap[1] := Heap[length];
            length := length - 1;
            max_heapify(Heap, 1, length);
            extract_maximum := max;
         end; { EndIf }
   end; { EndFunction }

procedure increase_value(var  Heap : ArrayHeap; i : integer; val : DataHeap);
   var
     element : DataHeap;

   begin
     Heap[i] := val;
     while((i > 1) and (Heap[i div 2].Prioritas < Heap[i].Prioritas)) do
        begin
           element := Heap[i div 2];
           Heap[i div 2] := Heap[i];
           Heap[i] := element;
           i := i div 2;
        end; { EndWhile }
   end; { EndProcedure }

procedure insert_value(var Heap : ArrayHeap; val : DataHeap; var length : integer);
   begin
      length := length + 1;
      //Heap[length]
      increase_value(Heap, length, val);
   end; { EndProcedure }

procedure TForm1.FormCreate(Sender: TObject);
begin
   length := 0;
   Nomor := 1;
   waveOutSetVolume(0,$90009000);
   Volume.Position := 10;
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
begin
   data.Nomor := Nomor;
   data.Tipe := 'B';
   data.Prioritas := 999 - Nomor; { Prioritas antrian bisnis lebih tinggi daripada personal }
   insert_value(Heap, data, length);
   Nomor := Nomor + 1;

   if(length > 0) then
      begin
         BitBtn1.Enabled := True;
         BitBtn2.Enabled := True;
      end;
end;

procedure TForm1.BitBtn3Click(Sender: TObject);
begin
   data.Nomor := Nomor;
   data.Tipe := 'P';
   data.Prioritas := -999 - Nomor; { Prioritas antrian personal lebih rendah daripada bisnis }
   insert_value(Heap, data, length);
   Nomor := Nomor + 1;
   if(length > 0) then
      begin
         BitBtn1.Enabled := True;
         BitBtn2.Enabled := True;
      end;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
   data := extract_maximum(Heap, length);
   if(data.Nomor < 10) then
      angka := '00' + IntToStr(data.Nomor)
   else
      if(data.Nomor < 99) then
         angka := '0' + IntToStr(data.Nomor)
      else
         if(data.Nomor > 99) then
            angka := IntToStr(data.Nomor);

   Label1.Caption := data.Tipe + '-' + angka;
   suara(1, data.Tipe, angka);
   if(length < 1) then
      begin
         BitBtn1.Enabled := False;
         BitBtn2.Enabled := False;
      end;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
   data := extract_maximum(Heap, length);
   if(data.Nomor < 10) then
      angka := '00' + IntToStr(data.Nomor)
   else
      if(data.Nomor < 99) then
         angka := '0' + IntToStr(data.Nomor)
      else
         if(data.Nomor > 99) then
            angka := IntToStr(data.Nomor);

   Label2.Caption := data.Tipe + '-' + angka;
   suara(2, data.Tipe, angka);
   if(length < 1) then
      begin
         BitBtn1.Enabled := False;
         BitBtn2.Enabled := False;
      end;
end;

end.
