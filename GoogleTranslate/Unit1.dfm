object Form1: TForm1
  Left = 437
  Top = 182
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Antrian Bank'
  ClientHeight = 264
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblSisaAntrian: TLabel
    Left = 16
    Top = 248
    Width = 68
    Height = 13
    Caption = 'Sisa Antrian: 0'
  end
  object lblAntrian: TLabel
    Left = 252
    Top = 248
    Width = 128
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nomor Antrian Sekarang: 0'
  end
  object TBVolume: TTrackBar
    Left = 64
    Top = 208
    Width = 129
    Height = 33
    Max = 20
    TabOrder = 0
    OnChange = TBVolumeChange
  end
  object CMute: TCheckBox
    Left = 16
    Top = 208
    Width = 49
    Height = 33
    Caption = 'MUTE'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Roboto Cn'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = CMuteClick
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 8
    Width = 177
    Height = 121
    BiDiMode = bdLeftToRight
    Caption = 'MEJA 1'
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Roboto Cn'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    object lblMeja1: TLabel
      Left = 32
      Top = 40
      Width = 100
      Height = 48
      Caption = 'B-000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -40
      Font.Name = 'Roboto Cn'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 200
    Top = 8
    Width = 177
    Height = 121
    Caption = 'MEJA 2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Roboto Cn'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    object lblMeja2: TLabel
      Left = 32
      Top = 40
      Width = 100
      Height = 48
      Caption = 'P-000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -40
      Font.Name = 'Roboto Cn'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GroupBox3: TGroupBox
    Left = 16
    Top = 136
    Width = 177
    Height = 65
    Caption = 'PANGGIL'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Roboto Cn'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object tblPanggilMeja1: TButton
      Left = 8
      Top = 24
      Width = 81
      Height = 25
      Caption = 'MEJA 1'
      Enabled = False
      TabOrder = 0
      OnClick = tblPanggilMeja1Click
    end
    object tblPanggilMeja2: TButton
      Left = 88
      Top = 24
      Width = 81
      Height = 25
      Caption = 'MEJA 2'
      Enabled = False
      TabOrder = 1
      OnClick = tblPanggilMeja2Click
    end
  end
  object GroupBox4: TGroupBox
    Left = 200
    Top = 136
    Width = 177
    Height = 105
    Caption = 'AMBIL ANTRIAN'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Roboto Cn'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    object tblBisnis: TButton
      Left = 16
      Top = 32
      Width = 73
      Height = 57
      Caption = 'BISNIS'
      TabOrder = 0
      OnClick = tblBisnisClick
    end
    object tblPersonal: TButton
      Left = 88
      Top = 32
      Width = 73
      Height = 57
      Caption = 'PERSONAL'
      TabOrder = 1
      OnClick = tblPersonalClick
    end
  end
end
