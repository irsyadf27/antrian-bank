unit Unit1;

interface

uses
  mmsystem, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, MPlayer, ExtCtrls;

type
  DataHeap = record
     Nomor, Prioritas : integer;
     Tipe : char;
  end;
  ArrayHeap = array[1..999] of DataHeap;

  TForm1 = class(TForm)
    TBVolume: TTrackBar;
    CMute: TCheckBox;
    GroupBox1: TGroupBox;
    lblMeja1: TLabel;
    GroupBox2: TGroupBox;
    lblMeja2: TLabel;
    GroupBox3: TGroupBox;
    tblPanggilMeja1: TButton;
    tblPanggilMeja2: TButton;
    GroupBox4: TGroupBox;
    tblBisnis: TButton;
    tblPersonal: TButton;
    lblSisaAntrian: TLabel;
    lblAntrian: TLabel;
    procedure TBVolumeChange(Sender: TObject);
    procedure tblBisnisClick(Sender: TObject);
    procedure CMuteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tblPersonalClick(Sender: TObject);
    procedure tblPanggilMeja1Click(Sender: TObject);
    procedure tblPanggilMeja2Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  heapList : ArrayHeap;
  dt : DataHeap;
  Nomor, currentSize : integer;

implementation

{$R *.dfm}
procedure terbilang( n : longint);
  const
    Bil : Array[0..11] Of String[15] =
     ('','satu.wav','dua.wav','tiga.wav','empat.wav','lima.wav',
      'enam.wav','tujuh.wav','delapan.wav','sembilan.wav','sepuluh.wav','sebelas.wav');
  begin
    case n Of
    0..11 : begin
          if(Bil[n] <> '') then
            PlaySound(PChar(PAnsiChar('suara/') + Bil[n]), 0, SND_SYNC or SND_FILENAME);
        end;
    12..19 : begin
            Terbilang(n-10);
            PlaySound('suara/belas.wav', 0, SND_SYNC or SND_FILENAME);
        end;
    20..99 : begin
            Terbilang(n div 10);
            PlaySound('suara/puluh.wav', 0, SND_SYNC or SND_FILENAME);
            Terbilang(n mod 10);
        end;
    100..199 : begin
            PlaySound('suara/seratus.wav', 0, SND_SYNC or SND_FILENAME);
            Terbilang(n-100);
        end;
    200..999 : begin
            Terbilang(n div 100);
            PlaySound('suara/ratus.wav', 0, SND_SYNC or SND_FILENAME);
            Terbilang(n mod 100);
        end;
  end;
end;

{ Heap : https://github.com/bnmnetp/pythonds/blob/master/trees/binheap.py }
function minChild(heapList : ArrayHeap; i : integer) : integer;
   begin
      if(i*2+1 > currentSize) then
         minChild := i * 2
      else
         if(heapList[i * 2].Prioritas < heapList[i * 2 + 1].Prioritas) then
            minChild := i * 2
         else
            minChild := i * 2 + 1;
   end;

procedure precUp(var heapList : ArrayHeap; i : integer);
   var
      tmp : DataHeap;
   begin
      while(i div 2 > 0) do
         begin
            if(heapList[i].Prioritas < heapList[i div 2].Prioritas) then
               begin
                  tmp := heapList[i div 2];
                  heapList[i div 2] := heapList[i];
                  heapList[i] := tmp;
               end;
            i := i div 2;
         end;
   end;

procedure precDown(var heapList : ArrayHeap; i : integer);
   var
      mc : integer;
      tmp : DataHeap;
   begin
      while((i * 2) <= currentSize) do
         begin
            mc := minChild(heapList, i);
            if(heapList[i].Prioritas >= heapList[mc].Prioritas) then
               begin
                  tmp := heapList[i];
                  heapList[i] := heapList[mc];
                  heapList[mc] := tmp;
               end;
            i := mc;
         end;
   end;

procedure insert(Data : DataHeap; var heapList : ArrayHeap; var currentSize : integer);
   begin
      currentSize := currentSize + 1;
      heapList[currentSize] := Data;
      precUp(HeapList, currentSize);
   end;

function delMin(var heapList : ArrayHeap; var currentSize : integer) : DataHeap;
   var
      retval : DataHeap;
   begin
      retval := heapList[1];
      heapList[1] := heapList[currentSize];
      currentSize := currentSize - 1;
      precDown(heapList, 1);
      delMin := retval;
   end;
   
procedure TForm1.TBVolumeChange(Sender: TObject);
begin
  Case TBVolume.Position of
     1: waveOutSetVolume(0,$00000000);
     2: waveOutSetVolume(0,$10001000);
     3: waveOutSetVolume(0,$20002000);
     4: waveOutSetVolume(0,$30003000);
     5: waveOutSetVolume(0,$40004000);
     6: waveOutSetVolume(0,$50005000);
     7: waveOutSetVolume(0,$60006000);
     8: waveOutSetVolume(0,$70007000);
     9: waveOutSetVolume(0,$80008000);
     10:waveOutSetVolume(0,$90009000);
     11:waveOutSetVolume(0,$A000A000);
     12:waveOutSetVolume(0,$B000B000);
     13:waveOutSetVolume(0,$C000C000);
     14:waveOutSetVolume(0,$D000D000);
     15:waveOutSetVolume(0,$E000E000);
     16:waveOutSetVolume(0,$F000F000);
     17:waveOutSetVolume(0,$FF00FF00);
     18:waveOutSetVolume(0,$FFF0FFF0);
     19:waveOutSetVolume(0,$FFFFFFFF);
     20:waveOutSetVolume(0,$FFFFFFFF);
  end;
end;

procedure TForm1.tblBisnisClick(Sender: TObject);
begin
  // Untuk Bisni menggunakan Prioritas lebih kecil agar selalu didepan
  dt.Prioritas := -999 + Nomor;
  dt.Nomor := Nomor;
  dt.Tipe := 'B';
  insert(dt, heapList, currentSize);
  Nomor := Nomor + 1;
  if(tblPanggilMeja1.Enabled = False) then
     tblPanggilMeja1.Enabled := True;
  if(tblPanggilMeja2.Enabled = False) then
     tblPanggilMeja2.Enabled := True;
  lblSisaAntrian.Caption := 'Sisa Antrian: ' + IntToStr(currentSize);
  lblAntrian.Caption := 'Nomor Antrian Sekarang: ' + IntToStr(Nomor - 1);
end;

procedure TForm1.tblPersonalClick(Sender: TObject);
begin
  dt.Prioritas := 999 + Nomor;
  dt.Nomor := Nomor;
  dt.Tipe := 'P';
  insert(dt, heapList, currentSize);
  Nomor := Nomor + 1;
  if(tblPanggilMeja1.Enabled = False) then
     tblPanggilMeja1.Enabled := True;
  if(tblPanggilMeja2.Enabled = False) then
     tblPanggilMeja2.Enabled := True;
  lblSisaAntrian.Caption := 'Sisa Antrian: ' + IntToStr(currentSize);
  lblAntrian.Caption := 'Nomor Antrian Sekarang: ' + IntToStr(Nomor - 1);
end;

procedure TForm1.CMuteClick(Sender: TObject);
begin
  If Cmute.Checked = True then
     begin
        TBVolume.Enabled := false;
        waveOutSetVolume(0,$00000000);
        TBVolume.Position := 0;
     end;
     if Cmute.Checked = False then
     begin
        TBVolume.Enabled := true;
        waveOutSetVolume(0,$90009000);
        TBVolume.Position := 10;
     end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  TBVolume.Position := 10;
  waveOutSetVolume(0,$90009000);
  Nomor := 1;
end;

procedure TForm1.tblPanggilMeja1Click(Sender: TObject);
begin
  dt := delMin(heapList, currentSize);
  if(dt.Nomor < 10) then
   lblMeja1.Caption := dt.Tipe + '-00' + IntToStr(dt.Nomor)
  else if(dt.Nomor <= 99) then
   lblMeja1.Caption := dt.Tipe + '-0' + IntToStr(dt.Nomor)
  else if(dt.Nomor > 99) then
   lblMeja1.Caption := dt.Tipe + '-' + IntToStr(dt.Nomor);
  if(currentSize < 1) then
    begin
      tblPanggilMeja1.Enabled := False;
      tblPanggilMeja2.Enabled := False;
    end;
  if(cmute.Checked = False) then
    begin
      PlaySound('suara/nomor-urut.wav', 0, SND_SYNC or SND_FILENAME);
      PlaySound(PChar(PAnsiChar('suara/') + dt.Tipe + PAnsiChar('.wav')), 0, SND_SYNC or SND_FILENAME);
      terbilang(dt.Nomor);
      PlaySound('suara/loket.wav', 0, SND_SYNC or SND_FILENAME);
      PlaySound('suara/satu.wav', 0, SND_SYNC or SND_FILENAME);
    end;
  lblSisaAntrian.Caption := 'Sisa Antrian: ' + IntToStr(currentSize);
end;

procedure TForm1.tblPanggilMeja2Click(Sender: TObject);
begin
  dt := delMin(heapList, currentSize);
  if(dt.Nomor < 10) then
   lblMeja2.Caption := dt.Tipe + '-00' + IntToStr(dt.Nomor)
  else if(dt.Nomor <= 99) then
   lblMeja2.Caption := dt.Tipe + '-0' + IntToStr(dt.Nomor)
  else if(dt.Nomor > 99) then
   lblMeja2.Caption := dt.Tipe + '-' + IntToStr(dt.Nomor);
  if(currentSize < 1) then
    begin
      tblPanggilMeja1.Enabled := False;
      tblPanggilMeja2.Enabled := False;
    end;
  if(cmute.Checked = False) then
    begin
      PlaySound('suara/nomor-urut.wav', 0, SND_SYNC or SND_FILENAME);
      PlaySound(PChar(PAnsiChar('suara/') + dt.Tipe + PAnsiChar('.wav')), 0, SND_SYNC or SND_FILENAME);
      terbilang(dt.Nomor);
      PlaySound('suara/loket.wav', 0, SND_SYNC or SND_FILENAME);
      PlaySound('suara/dua.wav', 0, SND_SYNC or SND_FILENAME);
    end;
  lblSisaAntrian.Caption := 'Sisa Antrian: ' + IntToStr(currentSize);
end;

end.
